function rotate_axes(h)
  global _catched _ref_info
  if or(h.user_data.eventHandlers=='plotlib_rotate_handler')
    return
  end
  _add_event_handler(get(h,'figure_id'),'plotlib_rotate_handler');
  _catched=[];
  _ref_info= h.info_message;
  h.info_message='Plotlib message: left-click on an axes to start rotation.';
endfunction
