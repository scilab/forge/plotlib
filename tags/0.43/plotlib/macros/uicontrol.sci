function varargout=uicontrol(varargin)

parent=get('current_figure');

_start=1;
if length(varargin)
   if typeof(varargin(1))=="pltlibH"
     if varargin(1).handle.type=="uicontrol" & length(varargin)==1
         scilab_uicontrol(varargin(1).handle);
         varargout(1)=varargin(1);
         return;
     elseif or(varargin(1).handle.type==['Figure';'uicontrol'])
       parent=varargin(1).handle;
       _start=2;
     else
        _error("uicontrol : handle should be an uicontrol or figure handle")
     end
   end     
end

style='pushbutton';
for i=_start:2:length(varargin)-1
  if convstr(varargin(i))=='parent'
    parent=varargin(i+1);
  elseif convstr(varargin(i))=='style'
    style=varargin(i+1);
  end
end

if typeof(parent)=='pltlibH'
  parent=parent.handle;
end  

win=parent;
while get(win,'type')<>'Figure'
  win=get(win,'parent');
end

IMD=get(win,'immediate_drawing');
set(win,'immediate_drawing','off');

h=scilab_uicontrol(parent,'style',style);

for i=_start:2:length(varargin)-1
  if and(convstr(varargin(i))<>['parent','style'])
    set(h,varargin(i),varargin(i+1));
  end  
end

varargout(1)=mlist(['pltlibH','handle'],h);

set(win,'immediate_drawing',IMD);

endfunction
