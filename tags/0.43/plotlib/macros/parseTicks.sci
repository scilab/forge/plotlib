function out=parseTicks(typeOfPlot,value,pptystring,ppty)

select type(value)

case 10 //  a string
  select value
    case 'off'
       out=list(ppty,[])
    case 'none'
       out=list(ppty,[])
    else
       _error(sprintf('%s : %s is an unknown ticks spec',typeOfPlot,value));
    end
case 1
    out=list(ppty,value);
else
   _error(sprintf('%s : missing ticks spec',typeOfPlot));
end
endfunction
