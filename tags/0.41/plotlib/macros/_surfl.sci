function hdl=_surfl(varargin)

[lhs,rhs]=argn(0);

if rhs==0
   [x,y]=meshgrid(-1:0.1:1,-1:0.1:1);
   hdl=_surfl(x,y,cos(%pi*x.*y),'facecolor','interp');
else
   hdl=_mainPlot('surfl',varargin);
end

endfunction /////
