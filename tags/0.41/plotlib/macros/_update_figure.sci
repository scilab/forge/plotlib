function _update_figure(_fig,argList)

global defaultFigureUserData

if argn(2)<3
  new=%f;
end

h=_fig.user_data;

i=1;

IMD=_fig.immediate_drawing;
_fig.immediate_drawing='off';

while i<= length(argList)

   ppty=argList(i);
   value=argList(i+1);

   if ppty=='Visible'

      _fig.visible=value;

   elseif ppty=='DoubleBuffer'

       _fig.pixmap=value;
       if value=="on"
         IMD="off";
       else
         IMD="on";  
       end

    elseif ppty=='Position'

       _fig.figure_position=value(1:2);
       _fig.axes_size=value(3:4);

    elseif ppty=='OuterPosition'

       _fig.figure_position=value(1:2);
       _fig.figure_size=value(3:4);
		  
    elseif ppty=='Color'

      _fig.background=findColorIndex(_fig,value);
      
    elseif ppty=='Name'

      _fig.figure_name=value;
	  
	elseif ppty=='Colormap'

      _colormap(_fig,value);
      h=_fig.user_data; // _colormap modifie _fig.user_data
   end
   h(ppty)=value; 
   i=i+2;
end

_fig.user_data=h;
_fig.immediate_drawing=IMD;

endfunction
