function _cla(varargin)

global defaultAxesUserData

%axesData_i_h=generic_i_h;  

[lhs,rhs]=argn();

cmd=[];
if length(varargin)<1
  ax=get('current_axes')
elseif length(varargin)<2
  if type(varargin(1))==10
    ax=get('current_axes')
    cmd=varargin(1);
  else
    ax=varargin(1);
  end
elseif length(varargin)<3
  ax=varargin(1);
  cmd=varargin(2);
end

if typeof(ax)=='pltlibH'
  ax=ax.handle  
end

if size(ax.children,1) 
  delete(ax.children);
end

ax.user_data.currentColor=1;

if cmd=="reset"
    if type(ax.user_data.legendHandle)~=1
        ierr=execstr('delete(ax.user_data.legendHandle)','errcatch');
        ax.user_data.legendType=0;
    end
    if type(ax.user_data.colorbarHandle)~=1
        ierr=execstr('delete(ax.user_data.colorbarHandle)','errcatch');
        ax.user_data.colorbarPosition="off";
        [Position,colorbarPosition]=changeVP(ax,ax.user_data.OuterPosition);
        ax.axes_bounds=Position;       
    end
    ax.user_data.XLimMode="auto";    
    ax.user_data.YLimMode="auto";    
    ax.user_data.ZLimMode="auto";    
    ax.user_data.CLimMode="auto";    
    ax.isoview="off";    
    ax.log_flags="nnn";
    ax.user_data.XScale='linear';    
    ax.user_data.YScale='linear';    
    ax.user_data.ZScale='linear';    
    ax.user_data.DataAspectRatio=[];    
    ax.cube_scaling="off";    
    ax.grid=[-1 -1 -1];    
    ax.user_data.XGrid="off";    
    ax.user_data.YGrid="off";    
    ax.user_data.ZGrid="off";    
    ax.x_label.text=' ';  
    ax.y_label.text=' '; 
    ax.z_label.text=' ';   
    ax.title.text=' ';    
    ax.user_data.ZGrid="off";    
end


endfunction
