function hdl=_trisurfl(varargin)

global PLOTLIB

[lhs,rhs]=argn(0);

if rhs==0
   load(PLOTLIB+'dinosaure.dat')
   h=gcf();
   IMD=h.immediate_drawing;
   h.immediate_drawing='off'
   hdl=_trisurfl(nodes,x,y,z)
   _axis equal
   _shading interp
   h.immediate_drawing=IMD;
else
   hdl=_mainPlot('trisurfl',varargin);
end

endfunction /////
