function _update_shaded_plots(win)
  
global _SHADED_ENTITIES
  
if win.type=="Figure"
  ax=win.children;
else
  ax=win;
end

for j=1:size(ax,1)
  if typeof(ax(j).user_data)=="axesData"
    h=ax(j).children;
    for i=1:size(h,1);
      if or(h(i).user_data.typeOfPlot==_SHADED_ENTITIES)      
         _update_single_shaded_pl(h(i));        
       end
    end
    if ax(j).user_data.colorbarPosition~="off"
       processColorBar(ax(j));
    end
  end
end



endfunction
