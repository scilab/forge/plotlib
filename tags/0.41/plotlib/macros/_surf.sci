function hdl=_surf(varargin)

[lhs,rhs]=argn(0);

if rhs==0
   [x,y]=meshgrid(-1:0.1:1,-1:0.1:1);
   hdl=_surf(x,y,cos(%pi*x.*y));
else
   hdl=_mainPlot('surf',varargin);
end

endfunction /////
