function graphinit(varargin)

global figurePropertiesNames axesPropertiesNames leafPropertiesNames ...
       markersTable lineStyleTable prefViewType reqMatrixArgs eventHandlers ...
       defaultFigureUserData defaultAxesUserData defaultLeafUserData ...
       _SHADED_ENTITIES _SURFACE_ENTITIES MAXCOL LINKS SCI5 ...
    
//
// Initialisation of graphic state. Must be called once at startup
//

[lhs,rhs]=argn(0);

// Initialisation of graphic state

_SHADED_ENTITIES=['pcolor','surf','surfl','tripcolor','trisurf','trisurfl','fill','fill3'];
_SURFACE_ENTITIES=[_SHADED_ENTITIES,'mesh','trimesh'];

MAXCOL=7; // Number of colors for edgecolor cycling (plot,plot3,etc.)

cmblack=[1 1 0; // We allocate a minimal colormap as a
  1 0.5 1;     // start for new figures
  0 1 1;
  1 .25 .25;
  .25 1 .25;
  .25 .25 1;
  .75 .75 .75;
  0 0 0;
  1 1 1];

cmwhite=[ 0 0 1;
  0 0.5 0;
  1 0 0;
  0 0.75 0.75;
  .75 0 .75;
  .75 .75 0;
  .25 .25 .25;
  0 0 0;
  1 1 1];
  
if rhs==1 // default background is white
   bg=varargin(1);
else
   bg=[1 1 1];
end


if (type(bg)==1)
      if length(bg)==3
         clum = ([.298936021 .58704307445 .114020904255]*bg(:) >= .5) + 1;
	 if clum==1
	    cmap = cmblack;
	    simpletable=tlist(['simpletable';'y';'m';'c';'b';'g';'r';'w';'k'],[1 1 0],[1 0.5 1],[0 1 1],[0.25 0.25 1],[0.25 1 0.25],[1 .25 .25],[1 1 1],[0 0 0]);
	  else
	    simpletable=tlist(['simpletable';'y';'m';'c';'b';'g';'r';'k';'w'],[0.75 0.75 0],[0.75 0 0.75],[0 0.75 0.75],[0 0 1],[0 0.5 0],[1 0 0],[0 0 0],[1 1 1]);
	    cmap = cmwhite;
	  end
      else
        error('graphinit : background color specification must be a 3-vector')
      end
else
      error('graphinit : background color specification must be a 3-vector')
end 

defaultColormap=jetcolormap(64);
cmap($+1:64,:)=%nan;

clum = ([.298936021 .58704307445 .114020904255]*bg' >= .5) + 1;
if clum==1
     fg = [1 1 1];
     fbg = 0.7*bg + .3*fg;
else
     fg=[0 0 0];
	 fbg=0.8*bg;
end

cmap(10,:)=fbg(:)';
cmap(11,:)=bg(:)';
cmap(12,:)=fg(:)';

figurePrivateProperties=[
'Colormaptable';
'simpleColorTable';
'margin';
'eventHandlers'];

figurePublicProperties=[
'OuterPosition'    'parsePosition'
'Position'         'parsePosition'
'Name'             'parseLabel'
'FixedColors'      'parseColormap'
'Colormap' 	 'parseColormap'
'Color'            'parseColor'
'DoubleBuffer'     'parseOnOff'
'Visible'          'parseOnOff'];

figurePropertiesNames=tlist(['figureProperties';convstr(figurePublicProperties(:,1))]);
for i=1:size(figurePublicProperties,1);
  figurePropertiesNames(convstr(figurePublicProperties(i,1)))=[figurePublicProperties(i,:)];
end

defaultFigureUserData=tlist(['figureData'
figurePrivateProperties
figurePublicProperties(:,1)],...
 [],...
 simpletable,...
 0.1,...
 [],...
 [],...
 [],...
'',...
 cmap,...
 defaultColormap,...
 fbg,...
 'off',...
 'on');
 

reqMatrixArgs=tlist(['reqMxArgs';
'plot';
'semilogx';
'semilogy';
'loglog';
'mesh';
'surf';
'surfl';
'pcolor';
'plot3';
'quiver';
'quiver3';
'triplot';
'tripcolor';
'trimesh';
'trisurf';
'trisurfl';
'fill';
'fill3'],...
[1;2],...
[1;2],...
[1;2],...
[1;2],...
[1;3],...
[1,2;3,4],...
[1,2;3,4],...
[1;3],...
[3],...
[2,3;4,5],...
[4,5;6,7],...
[3],...
[4],...
[4],...
[4,5],...
[4],...
[3],...
[4]);

prefViewType=tlist(['prefViewType';
'plot';
'semilogx';
'semilogy';
'loglog';
'mesh';
'surf';
'surfl';
'pcolor';
'plot3';
'quiver';
'quiver3';
'triplot';
'tripcolor';
'trimesh';
'trisurf';
'trisurfl';
'fill';
'fill3'],...
'2d',...
'2d',...
'2d',...
'2d',...
'3d',...
'3d',...
'3d',...
'2d',...
'3d',...
'2d',...
'3d',...
'2d',...
'2d',...
'3d',...
'3d',...
'3d',...
'2d',...
'3d');

leafPrivateProperties=[
'typeOfPlot'
'parent'
'data_bounds'
'light'
'child'
'vertex_indices'];

leafPublicProperties=[
'Faces'            'parseData'
'FaceVertexCData'  'parseData'
'Vertices'         'parseData'
'XData'            'parseData'
'YData'            'parseData'
'ZData'            'parseData'
'CData'            'parseData'
'CDataMapping'     'parseCDataMapping'
'UData'            'parseData'
'VData'            'parseData'
'WData'            'parseData'
'VertexNormals'    'parseData'
'FaceNormals'      'parseData'
'Color'            'parseColor'
'FaceColor'        'parseColor'
'EdgeColor'        'parseColor'
'LineWidth'        'parseLineStyle'
'LineStyle'        'parseLineStyle'
'Marker'           'parseMarker'
'MarkerSize'       'parseMarker'
'MarkerEdgeColor'  'parseColor'
'MarkerFaceColor'  'parseColor'
'AutoScaleFactor'  'parseFactor'
'DisplayName'      'parseLabel'];

leafPropertiesNames=tlist(['leafProperties';convstr(leafPublicProperties(:,1))]);
for i=1:size(leafPublicProperties,1);
  leafPropertiesNames(convstr(leafPublicProperties(i,1)))=[leafPublicProperties(i,:)];
end

defaultLeafUserData=tlist(['leafData'
leafPrivateProperties
leafPublicProperties(:,1)],...
[],[],%inf*[1 -1 1 -1 1 -1 1 -1],[-1 -1 1]',[],[],...
[],[],[],[],[],[],[],'scaled',[],[],[],[],[],...
'default','default','default',...
1,'default',...
'none',6,'default','none',0.9,'');

axesPrivateProperties=[
'legendHandle'
'legendType'
'colorbarHandle'
'colorbarPosition'
'currentColor'
'currentLineStyle'
'previousPosition'
'data_bounds'
'pretty_data_bounds'];

axesPublicProperties=[
'OuterPosition'       'parsePosition'
'Position'            'parsePosition'
'nextPlot'            'parseNextPlot'
'XDir'                'parseDir'
'YDir'                'parseDir'
'ZDir'                'parseDir'
'XColor'              'parseColor'
'YColor'              'parseColor'
'ZColor'              'parseColor'
'Color'               'parseColor'
'XTick'               'parseTicks'
'YTick'               'parseTicks'
'ZTick'               'parseTicks'
'XTickLabel'          'parseLabel'
'YTickLabel'          'parseLabel'
'ZTickLabel'          'parseLabel'
'XLabel'              'parseLabel'
'YLabel'              'parseLabel'
'ZLabel'              'parseLabel'
'Title'               'parseLabel'
'XGrid'               'parseOnOff'
'YGrid'               'parseOnOff'
'ZGrid'               'parseOnOff'
'XGridColor'          'parseColor'
'YGridColor'          'parseColor'
'ZGridColor'          'parseColor'
'DataAspectRatio'     'parseRatio'
'DataAspectRatioMode' 'parseLim'
'CLim'                'parseLim'
'XLim'                'parseLim'
'YLim'                'parseLim'
'ZLim'                'parseLim'
'CLimMode'            'parseLim'
'XLimMode'            'parseLim'
'YLimMode'            'parseLim'
'ZLimMode'            'parseLim'
'XScale'              'parseScale'
'YScale'              'parseScale'
'ZScale'              'parseScale'
'ColorOrder'          'parseColormap'
'LineStyleOrder'      'parseLineStyle'
'Visible'             'parseOnOff'
'XAxisLocation'       'parseAxisLocation'
'YAxisLocation'       'parseAxisLocation'
'ZAxisLocation'       'parseAxisLocation'
'Box'                 'parseOnOff'
'View'                'parseView'
'Tag'                 'parseLabel'];

defaultAxesUserData=tlist(['axesData'
axesPrivateProperties
axesPublicProperties(:,1)],...
[],1,[],'off',[1 1],1,[0 0 1 1],[0 1 0 1 0 1 0 1],[0 1 0 1 0 1 0 1],...
[0 0 1 1],[0.1 0.1 0.8 0.8],'erase','normal','normal','normal',...
fg,fg,fg,bg,...
'auto','auto','auto',...
'auto','auto','auto',...
'','','','',...
'off','off','off',...
fg,fg,fg,...
[1 1 1],'auto',...
[0 1],[0 1],[0 1],[0 1],...
'auto','auto','auto','auto',...
'linear','linear','linear',...
cmap(1:7,:),'-','on','bottom','left','left','on',[0 90],'');

axesPropertiesNames=tlist(['axesProperties';convstr(axesPublicProperties(:,1))]);
for i=1:size(axesPublicProperties,1);
  axesPropertiesNames(convstr(axesPublicProperties(i,1)))=[axesPublicProperties(i,:)];
end
  
ax=gda();
ax.margins=[0 0 0 0];
ax.axes_bounds=[0.1 0.1 0.8 0.8];
// ax.axes_bounds=[0 0 1 1]; fait planter Scilab gtk 4.2
ax.background=11;
ax.foreground=12;
ax.hidden_axis_color=12;
ax.font_color=12;
ax.axes_visible='off';
ax.visible='on';
ax.filled='off';
ax.box='off';
ax.clip_state='on';
ax.tight_limits='on';
ax.user_data=defaultAxesUserData;

win=gdf();
win.color_map=defaultFigureUserData.FixedColors;
win.background=10;
win.user_data=defaultFigureUserData;

if exists('xmltojar')// Scilab version > 5
  SCI5=%t;
  win.event_handler='plotlib_handler';
  win.event_handler_enable='on';
else
  SCI5=%f;
end


eventHandlers=list();

if LINKS==[]
  LINKS=list();
end


// end of graphinit



endfunction
