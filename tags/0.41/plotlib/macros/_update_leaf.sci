function _entity=_update_leaf(h,argList)
  
global _SURFACE_ENTITIES defaultLeafUserData

i=1;
new=%t;
_entities=[];

if type(h)==9
  new=%f;
  _entity=h;
  h=_entity.user_data;
end

changed_properties=[];

if argn(2)==2
  while i<=length(argList)
    changed_properties=[changed_properties;argList(i)];
    h(argList(i))=argList(i+1)
    i=i+2;
  end
end

ax=h.parent;
_win=ax.parent;

IMD=_win.immediate_drawing;
_win.immediate_drawing="off";

if new
  sca(ax);  
end

if or(h.typeOfPlot==["plot";"semilogx";"semilogy";"loglog";"plot3"])

  if new
    if h.ZData==[]
      h.ZData=0*h.YData
    end
    if h.typeOfPlot=='plot3'
      param3d1(h.XData,h.YData,h.ZData,flag=[0,0]); 
    else
      plot2d(h.XData,h.YData,frameflag=0);  
    end    
    _entity=gce();
    if _entity.type=="Compound"
      _entity=_entity.children;
    end
     
    for i=size(_entity,1):-1:1        
        hi=h;
        hi.XData=h.XData(:,i);
        hi.YData=h.YData(:,i);
        hi.ZData=h.ZData(:,i);
        hi.CData=h.CData(:,i);
        hi.data_bounds=[min(hi.XData) min(hi.YData) min(hi.ZData) min(hi.CData)
                        max(hi.XData) max(hi.YData) max(hi.ZData) max(hi.CData)];
        hi=cycleColorsLineStyle(hi); 
        _entity(i).user_data=hi;
        _entity(i).data(:,3)=hi.ZData(:);
        setMarkAndLineProperties(_entity(i),hi);
    end
    _win.immediate_drawing=IMD;
    return        
  else
    set(_entity,"data",[h.XData(:) h.YData(:) h.ZData(:)]);
  end
                                 
elseif or(h.typeOfPlot==["quiver";"quiver3"])

  if h.AutoScaleFactor==0
    _scale=1;
  else
    if isvector(h.XData)
      dx=max(abs(h.XData(1:$-1)-h.XData(2:$)));
      dy=max(abs(h.YData(1:$-1)-h.YData(2:$)));
      dz=max(abs(h.ZData(1:$-1)-h.ZData(2:$)));
    else
      dx=max(abs(h.XData(1:$-1,1:$-1)-h.XData(2:$,2:$)));
      dy=max(abs(h.YData(1:$-1,1:$-1)-h.YData(2:$,2:$)));
      dz=max(abs(h.ZData(1:$-1,1:$-1)-h.ZData(2:$,2:$)));
    end
    _scale=sqrt((dx^2+dy^2)/max(h.UData.^2+h.VData.^2+h.WData.^2))*h.AutoScaleFactor;
  end
    
  nx=[h.XData(:)';h.XData(:)'+h.UData(:)'*_scale];	
  ny=[h.YData(:)';h.YData(:)'+h.VData(:)'*_scale];	
  nz=[h.ZData(:)';h.ZData(:)'+h.WData(:)'*_scale];

  if new
    h=cycleColorsLineStyle(h);
    xarrows(nx,ny,-1);	
    _entity=gce();
    _entity.data(:,3)=nz(:);
    _entity.arrow_size=0.5;
    param3d(0,0,0,flag=[0 0]);
    _child=gce();
    _child.visible="off";
    _child.polyline_style=4;
    _child.arrow_size_factor=0.7;
    _child.user_data=defaultLeafUserData;
    plot3d1(h.XData,h.YData,h.ZData,flag=[0 0 0]);
    _marks=gce();
    _marks.surface_mode="off";
    _entity=glue([_child _marks _entity]);    
  else
    set(_entity.children(1),'data',[nx(:) ny(:) nz(:)]);
  end
  setMarkAndLineProperties(_entity,h);
  h.data_bounds=[min(nx) min(ny) min(nz) 0;max(nx) max(ny) max(nz) 0];
  _entity.user_data=h;
  _win.immediate_drawing=IMD;
  return
    
  elseif  or(h.typeOfPlot==_SURFACE_ENTITIES)
    
   if new | or(changed_properties==['XData','YData','ZData','Faces','Vertices']) 
     
     
     if or(h.typeOfPlot==["fill" "fill3"])
       _X=h.XData($:-1:1,:);
       _Y=h.YData($:-1:1,:);
       _Z=h.ZData($:-1:1,:);
       indices=1:length(h.CData);
       if isvector(h.CData)
         indices=indices(ones(1,size(h.XData,1)),:);
       else
         indices=matrix(indices,size(h.CData));
       end
       h.vertex_indices=indices($:-1:1,:)

     elseif or(h.typeOfPlot==["trisurf";"trisurfl";"tripcolor";"trimesh"])
       
         
        indices=matrix(1:length(h.FaceVertexCData),size(h.FaceVertexCData));    
         [_X,_Y,_Z]=generate3dPolygons(h.Faces,h.Vertices(:,1),h.Vertices(:,2),h.Vertices(:,3)+%i*indices,3,~isvector(h.Vertices(:,1)));
        indices=imag(_Z);
       
        _Z=real(_Z);
        ntri=size(h.Faces,2);
        nnodes=size(h.Vertices,1);
                
         N=-[(_Y(2,:)-_Y(1,:)).*(_Z(3,:)-_Z(1,:))-(_Z(2,:)-_Z(1,:)).*(_Y(3,:)-_Y(1,:));
        (_Z(2,:)-_Z(1,:)).*(_X(3,:)-_X(1,:))-(_X(2,:)-_X(1,:)).*(_Z(3,:)-_Z(1,:));
        (_X(2,:)-_X(1,:)).*(_Y(3,:)-_Y(1,:))-(_Y(2,:)-_Y(1,:)).*(_X(3,:)-_X(1,:))];
  
        N=N*sparse([1:ntri;1:ntri]',1../(%eps+sqrt(sum(N.^2,'r'))));
        
        h.FaceNormals=N;
        
        _N=zeros(3,nnodes);
        nV=zeros(nnodes,1);
        
        for i=1:ntri
          nodes=h.Faces(:,i);        
          _N(:,nodes)=_N(:,nodes)+N(:,[i i i]);
          nV(nodes)=nV(nodes)+1;
        end
        clear N
        h.VertexNormals=_N*sparse([1:nnodes;1:nnodes]',1../(nV+%eps));
        clear _N;
        
        h.XData=_X;h.YData=_Y;h.ZData=_Z;
        
        if h.typeOfPlot=='trisurfl'
          h.FaceVertexCData=matrix(computeLight(h.VertexNormals,h.light(:)),size(h.Vertices(:,1)));
        end
        h.CData=matrix(h.FaceVertexCData(indices),size(h.ZData));
        h.vertex_indices=matrix(1:length(h.CData),size(h.CData));
                
     else // surf, surfl, pcolor  
       
       indices=matrix(1:length(h.CData),size(h.CData));    
       [_X,_Y,_Z]=generate3dPolygons(h.Faces,h.XData,h.YData,h.ZData+%i*indices,4,~isvector(h.XData));      
       h.vertex_indices=imag(_Z);
     
       _Z=real(_Z);
       if isvector(h.XData)
         [zx,zy]=nonParametricDiffData(h.XData,h.YData,h.ZData);
         h.VertexNormals=nonParametricNormals(zx,zy);
       else
         [xu,yu,zu,xv,yv,zv]=parametricDiffData(h.XData,h.YData,h.ZData);
         h.VertexNormals=parametricNormals(xu,yu,zu,xv,yv,zv);
       end
       if h.typeOfPlot=='surfl'          
         h.CData=matrix(computeLight(h.VertexNormals,h.light(:)),size(h.ZData));
       end 
     end      
   end
     
   if new 
      plot3d1(_X,_Y,list(_Z,0*_Z),flag=[0 0 0],alpha=ax.rotation_angles(1),theta=ax.rotation_angles(2));
      _entity=gce();
   elseif or(changed_properties==['XData','YData','ZData','Faces','Vertices']) 
      _entity.data=tlist(["3d";"x";"y";"z";"color"],_X,_Y,_Z,_entity.data.color);
   end

   if new | or(changed_properties==['FaceColor','EdgeColor'])
     setColors(_entity,h);
   end
   
end
  
h.data_bounds=[min(h.XData) min(h.YData) min(h.ZData) min(h.CData)
max(h.XData) max(h.YData) max(h.ZData) max(h.CData)];  
_entity.user_data=h;

if new | or(changed_properties==['Color','LineStyle','LineWidth','Marker','MarkerFaceColor','MarkerEdgeColor'])
    setMarkAndLineProperties(_entity,h);
end

if ~new | or(changed_properties==['XData','YData','ZData','CData','Vertices']) 
  _compute_data_bounds(ax);  
end            

_win.immediate_drawing=IMD;

endfunction

  
