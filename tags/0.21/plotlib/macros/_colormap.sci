function c=_colormap(varargin)

c=[];

[lhs,rhs]=argn(0);

win=safeInit();

if rhs==0
   c=win.user_data.RGBcolormap;
elseif rhs==1
   _fig(win.figure_id,'colormap',varargin(1));
else
   error('colormap : two many input arguments');
end

endfunction
