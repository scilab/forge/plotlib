function hdl=addPlot(_type,X,Y,Z,c,markerId,sizes,lineStyle,ax,_factor)


// Adds a quadruple to the list of elementary plots to do

[lineColorIndices,markerForegroundIndices,markerBackgroundIndices]=cycleColors(ax,c,size(X,2));

if _type=="plot3"

    if ax.children==[]
      ax.data_bounds=[min(X) min(Y) min(Z);max(X) max(Y) max(Z)];
    end
    param3d1(X,Y,list(Z,lineColorIndices));
    h=gce();

elseif _type=="quiver"

    if ax.children==[]
      ax.data_bounds=[min(X) min(Y);max(X) max(Y)];
    end

    u=real(Z);
    v=imag(Z);
    nx=zeros(2,length(X));
    ny=zeros(2,length(X));

    if _factor==0
      _scale=1;
    else
      dx=max(abs(X(1:$-1,1:$-1)-X(2:$,2:$)));
      dy=max(abs(Y(1:$-1,1:$-1)-Y(2:$,2:$)));
      _scale=sqrt((dx^2+dy^2)/max(u.^2+v.^2))*_factor;
    end
      

    nx(1,:)=X(:)';
    nx(2,:)=X(:)'+u(:)'*_scale;	

    ny(1,:)=Y(:)';
    ny(2,:)=Y(:)'+v(:)'*_scale;	
	
    ax.clip_state="on";
    xarrows(nx,ny,-1,lineColorIndices(1));	
	h=gce();
	h.arrow_size=0.5;
	h.thickness=1;

else

    if ax.children==[]
      ax.data_bounds=[min(X) min(Y);max(X) max(Y)];
    end
    plot2d(X,Y,style=lineColorIndices);
    h=get(gce(),'children');

end

if lineStyle==0
    set(h,'line_mode','off');
else
    set(h,'line_style',lineStyle);
end

set(h,'thickness',sizes(1));

if markerId ~=[]
    set(h,'mark_mode','on');
    set(h,'mark_style',markerId);    
    set(h,'mark_size_unit','point');    
    set(h,'mark_size',6+3*(sizes($)-1));  
    for i=1:size(h,1)
        set(h(size(h,1)-i+1),'mark_foreground',markerForegroundIndices(i));
    end
    if lineStyle==0
        for i=1:size(h,1)
            set(h(size(h,1)-i+1),'mark_background',lineColorIndices(i));
        end
	else
		for i=1:size(h,1)
            set(h(size(h,1)-i+1),'mark_background',markerBackgroundIndices(i));
        end
    end
end

hdl=h;

endfunction
