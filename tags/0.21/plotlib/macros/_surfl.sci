function hdl=_surfl(varargin)

[lhs,rhs]=argn(0);

if rhs==0
   x=-1:0.1:1;
   y=x;
   deff('z=f(x,y)','z=cos(%pi*x.*y)');
   hdl=_surfl(x,y,f,'facecolor','interp');
else
   hdl=mainPlot3d('surfl',varargin);
end

endfunction /////
