function hdl=_quiver(varargin)

[lhs,rhs]=argn(0);

if ~rhs
	[X,Y]=meshgrid(linspace(-1,1,20),linspace(-1,1,20));
	hdl=_quiver(X,Y,Y,-X);
   return
end

hdl=mainPlot3d('quiver',varargin);

// end of quiver
endfunction
