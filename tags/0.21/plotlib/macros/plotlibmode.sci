function plotlibmode()


functions=[
'fig'
'cla'
'clf'
'hold'
'axis'
'subplot'
'caxis'
'shg'
'colormap'
'colorbar'
'legend'
'xlabel'
'ylabel'
'zlabel'
'title'
'plot'
'plot3'
'fill'
'fill3'
'semilogx'
'semilogy'
'loglog'
'pcolor'
'tripcolor'
'quiver'
'mesh'
'surf'
'surfl'
'trimesh'
'trisurf'
'trisurfl'
'shading'];

list_out=sprintf('%s,',functions);
list_out=part(list_out,1:length(list_out)-1);
list_in=sprintf('_%s,',functions);
list_in=part(list_in,1:length(list_in)-1);

funcprot(0)
execstr(sprintf("[%s]=resume(%s);",list_out,list_in));


endfunction
