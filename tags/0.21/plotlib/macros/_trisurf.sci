function hdl=_trisurf(varargin)

[lhs,rhs]=argn(0);

if rhs==0 
   load(PLOTLIB+'dinosaure.dat')
   hdl=_trisurf(nodes,x,y,z,'axis','equal','shading','interp','colorbar','on')
else
   hdl=mainPlot3d('trisurf',varargin);
end

endfunction /////
