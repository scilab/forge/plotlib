mode(-1)

clear

predef clear

path=get_absolute_file_path('loader.sce')
[unit,err]=mopen(path+'macros/lib','r');
mclose(unit);
if err
   exec('builder.sce',-1)
end
load(path+'macros/lib')
disp('loading plotlib version '+plotlibver())

if ~exists('PLOTLIB')
  scilab_clf=clf;
  scilab_xlabel=xlabel;
  scilab_ylabel=ylabel;
  scilab_zlabel=zlabel;
  scilab_title=title;
  scilab_legend=legend;
end


PLOTLIB=path+'macros/';

if exists('xmltojar')
    add_help_chapter('Plotlib',path+'jar');
else
    add_help_chapter('Matlab-like plotting library',path+'man');
end

clear path Title unit err startup startup_path

%figData_i_h=generic_i_h;
%axisData_i_h=generic_i_h;

plotlibmode

predef all
if length(file())>2
	startup=2
end

