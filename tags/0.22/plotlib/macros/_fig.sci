function varargout=_fig(varargin) 

global defaultFigureUserData SCI5

if type(defaultFigureUserData)==1 // cannot compare a tlist with [] !
	graphinit();
end

cmap=[];
newWin=%F;

[lhs,rhs]=argn(0);
if rhs==0           // put the "first avalaible" window number at the
  if winsid()==[]   // top of the argument list
     winNum=0;
  else
     winNum=max(winsid())+1;
  end
  argList=list(winNum)
  newWin=%T;
else
   argList=varargin;
   if (type(argList(1))~=1)
      if winsid()==[]   // top of the argument list
         winNum=0;
       else
         winNum=max(winsid())+1;
       end
       argList(0)=winNum;
	   newWin=%T;
   else
   	   if and(winsid()~=argList(1)) & and(driver()~=['GIF','PPM','Pos'])
		      winNum=argList(1);
		      newWin=%T;
	     end
   end
end

while length(argList)

   if (type(argList(1))==1) // The argument seems to be a window number

      if length(argList(1)) ~= 1
         error('fig : figure number must be an integer scalar')
      end
      winNum=int(argList(1));

      if or(winsid()==winNum) // if winNum is the number of an existing window
         win=scf(winNum); // activate the window
         dontClear=%T;
      else
         win=scf(winNum); // create the window
         win.user_data=defaultFigureUserData;
	     newWin=%T;
      end
      argList(1)=null();
   elseif (type(argList(1))==10) // If this argument is a string

      select argList(1) // Try to identify a property name

      case 'background'

         win.user_data.background=parseColor('fig','background',argList);
         argList(1)=null(); argList(1)=null();

      case 'foreground'

         win.user_data.foreground=parseColor('fig','foreground',argList);
         argList(1)=null(); argList(1)=null();

      case 'frameColor'

         win.user_data.frameColor=parseColor('fig','frameColor',argList);
         argList(1)=null(); argList(1)=null();

      case 'color'

         win.user_data.frameColor=parseColor('fig','color',argList);
         argList(1)=null(); argList(1)=null();

      case 'colormap' // the user wants to change the RGB colormap

	     cmap=parseColormap('fig',argList);
         win.user_data.RGBcolormap=[];
         win.user_data.RGBcolormap=cmap;		 
         argList(1)=null(); argList(1)=null();

	case 'margin' // margin of plot box

		win.user_data.margin=parseMargin('fig',argList);
		argList(1)=null();argList(1)=null();
    
    case 'reset'
    
        argList(1)=null();
        win.user_data=defaultFigureUserData;
        newWin=%t;
    else

          str=sprintf('fig : %s is an unknown property name',argList(1));
          error(str)

    end // select argList(1)

   else
 
      str=sprintf('fig : argument %d has not the expected type',argNumber);
      error(str);

   end // if type(argList(1))

end // while length(argList)

if cmap~=[]
	IMD=win.immediate_drawing;
	win.immediate_drawing="off";
	RGBmap=win.user_data.RGBcolormap;
	n1=size(win.user_data.colormap,1);
	n2=size(RGBmap,1);
	ud=win.user_data;
	ud.RGBcolormaptable=n1+1:n1+n2;
	win.user_data=ud;
	win.color_map=[win.user_data.colormap;RGBmap];
	win.background=findColorIndex(win.user_data.frameColor');
	win.immediate_drawing=IMD;
end



if newWin
	clearWindow(win,'fig');
	seteventhandler('plotlib_handler');
	if SCI5
		win.event_handler_enable="on";
	end
end


varargout(1)=win;

// end of _fig
endfunction
