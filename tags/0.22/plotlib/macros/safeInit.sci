function win=safeInit()

global defaultFigureUserData DONE IMD SCI5

if and(driver()~=['GIF','PPM','Pos']) & (winsid()==[])
  _fig()
end

win=gcf(); // get the handle of current graphic window
ax=gca();

if type(win.user_data)==1
    if type(defaultFigureUserData)==1 // cannot compare a tlist with [] !
	    graphinit();
		seteventhandler(win.figure_id,'plotlib_handler');
		if SCI5
			win.event_handler_enable="on";
		end
    end    
    win.user_data=defaultFigureUserData;
    IMD=win.immediate_drawing;
    DONE=%t;
end

if type(ax.user_data)==1
    clearWindow(win,'clf');
end

endfunction
