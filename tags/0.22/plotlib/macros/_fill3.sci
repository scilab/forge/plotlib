function hdl=_fill3(varargin)

[lhs,rhs]=argn(0);

if rhs==0
  x=[0 1 1 0]';y=[0 0 1 1]';c=[1 2 3 4]';
  hdl=_fill3([x 1+x x],[y y 1+y],[x y y],[c 2+c 3+c],'facecolor','interp','axis','equal','view',[12 62]);
else
   hdl=mainPlot3d('fill3',varargin);
end

endfunction /////
