function _title(labelString)
    win=safeInit();
    ax=gca();
    ax.title.text=labelString;
    ax.title.font_foreground=ax.foreground;
endfunction
