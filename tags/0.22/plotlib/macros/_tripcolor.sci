function hdl=_tripcolor(varargin)

global PLOTLIB

[lhs,rhs]=argn(0);

if rhs==0
   load(PLOTLIB+'tridem.dat')
   _clf;
   hdl=_tripcolor(nodes,xy(1,:),xy(2,:),P(:,1),'shading', ...
   'interp','view',[135 60],'colorbar','on','axis','equal')
else
   hdl=mainPlot3d('tripcolor',varargin);
end

endfunction /////
