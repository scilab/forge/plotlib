function hdl=_plot(varargin)

[lhs,rhs]=argn(0);

if ~rhs
   _clf();
   drawlater();
   t=linspace(0,2*%pi,64);
   _subplot(2,2,1); 
   _plot(t,cos(t),'title','plot(t,cos(t))'); 
   _subplot(2,2,2); 
   _plot(t,[cos(t);sin(t);sin(t).*cos(t)],'title','plot(t,[cos(t);sin(t);sin(t).*cos(t)])');
   _subplot(2,2,3); 
   _plot(t,cos(t),t,cos(t),'og','background',[0 0 1],'title','plot(t,cos(t),t,cos(t),''og'',''background'',[0 0 1])');
   _subplot(2,2,4); 
   hdl=_plot(cos(t),sin(t),'axis','equal','title','plot(cos(t),sin(t),''axis'',''equal'')');
   drawnow();
   return
end

varargin(0) = 'linear';
varargin(0) = 'Xscale';
varargin(0) = 'linear';
varargin(0) = 'Yscale';

hdl=mainPlot('plot',varargin);

// end of plot
endfunction
