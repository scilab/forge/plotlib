function [vect]=caxis(varargin) 

[lhs,rhs]=argn(0);
vect=[];

i=1;
ax=get('current_axes');
while i<=length(varargin)
   if typeof(varargin(i))=='pltlibH'
     if varargin(i).handle.type=='Axes'
      ax=varargin(i).handle;
     else
      _error("caxis : handle should be an axes handle")
     end
     i=i+1;
   elseif type(varargin(i))==10
     out=parseCaxis("caxis",varargin(i),"climmode","CLimMode")
     i=i+1;
     break;
   elseif type(varargin(i))==1
     out=parseCaxis("caxis",varargin(i),"clim","CLim")
     i=i+1;
     break;
   end 
end

if i<= length(varargin)
   _error("caxis : too many input arguments")
end

if ~exists("out","local")
  vect=ax.user_data.CLim
  return;
end

_update_axes(ax,out);

endfunction

