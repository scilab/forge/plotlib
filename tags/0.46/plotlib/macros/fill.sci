function hdl=fill(varargin)

[lhs,rhs]=argn(0);

if rhs==0
  x=0.1+[0 1 1 0]';y=0.1+[0 0 1 1]';c=[1 2 3 4]';
  hdl=fill([x 1.1+x 1.1+x x],[y y 1.1+y 1.1+y],[c 2+c 3+c 4+c],'facecolor','interp');
else    
  hdl=_mainPlot('fill',varargin);
end

endfunction /////
