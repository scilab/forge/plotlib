function _update_shaded_plots(win)
  
global %plotlib_global
  
if win.type=="Figure"
  ax=win.children;
else
  ax=win;
end

for j=1:size(ax,1)
  if typeof(ax(j).user_data)=="axesData"
    h=getvalidchildren(ax(j))
    for i=1:size(h,1);
        if ~isempty(h(i).user_data)
            if or(h(i).user_data.typeOfPlot==[%plotlib_global._SHADED_ENTITIES,'image'])     
                _update_single_shaded_pl(h(i));        
            end
       end
    end
    if ax(j).user_data.colorbarPosition~="off"
       processColorBar(ax(j));
    end
  end
end

endfunction

function h=getvalidchildren(A)
  h=[]
  for k=1:size(A,'*')
    a=A(k)
    select a.type
    case "Fac3d" then
      h=[h;a]
    case "Matplot" then
      h=[h;a]
     case 'Axes'
      ax=a.children
      h=[h;getvalidchildren(ax)]
    case 'Compound'
     for k=1:1:size(a.children,'*')
	h=[h;getvalidchildren(a.children(k))]

      end
    end
  end
endfunction
