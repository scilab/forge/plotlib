function _del_event_handler(win,_function_name)

global %plotlib_global

if type(win)==1
  h=scf(win);
else
  h=win;
  win=h.figure_id;
end

_ind=find(h.user_data.eventHandlers==_function_name);
if _ind~=[]
  h.user_data.eventHandlers(_ind)=[];
  %plotlib_global.eventHandlers(win+1)=h.user_data.eventHandlers;
end

endfunction
