function hdl=plot3(varargin)

[lhs,rhs]=argn(0);

if rhs==0
  h=gcf();  
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';    
  t = linspace(0,5*%pi,100); 
  hdl=plot3(sin(t),cos(t),t/t($),...
  2+sin(t),cos(t),t/t($),... 
  sin(t),2+cos(t),t/t($),... 
  2+sin(t),2+cos(t),t/t($));
  h.immediate_drawing=IMD;    
else
   hdl=_mainPlot('plot3',varargin);
end

endfunction /////

