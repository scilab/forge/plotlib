function [marg] = parseMargin(funcName, value,pptystring,ppty)

//
// Parsing function for the 'margin' property 
//


select type(value)
case 1 // a matrix (must be a scalar)
   if (length(value)==1) & (value>=0) & (value<.5)
      marg=list(ppty,value);
   else
      _error(sprintf('%s : margin value must be a positive scalar smaller than .5',funcName));
   end
else
    _error(sprintf('%s : missing margin value',funcName));
end
endfunction
