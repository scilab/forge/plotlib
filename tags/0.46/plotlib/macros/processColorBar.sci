function processColorBar(ax)

win=ax.parent;
typ=ax.user_data.colorbarPosition;

cax=ax.user_data.CLim;

activateRGBColormap(win);

table=win.user_data.Colormaptable;
h=ax.user_data.colorbarHandle;
sca(h);
visible=h.visible;

execstr("delete(h.children)","errcatch");

if typ=='left' | typ=='right'
    xfpolys([0 1 1 0]',[cax(1) cax(1) cax(2) cax(2)]',[table(1) table(1) table($) table($)]');
    h.data_bounds=[0 cax(1);1 cax(2)];
    h.axes_visible(1)='off';
    h.axes_visible(2)='on';
    h.y_location=typ;
elseif typ=='top' | typ=='bottom'
    xfpolys([cax(1) cax(2) cax(2) cax(1)]',[0 0 1 1]',[table(1) table($) table($) table(1)]');
    h.data_bounds=[cax(1) 0;cax(2) 1];
    h.axes_visible(1)='on';
    h.axes_visible(2)='off';
    h.x_location=typ;
end

h.tight_limits='on';
h.box='on';
h.foreground=ax.foreground;
h.font_color=ax.font_color;
h.visible=visible;

sca(ax)

endfunction
