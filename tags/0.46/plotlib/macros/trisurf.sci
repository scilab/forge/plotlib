function hdl=trisurf(varargin)

[lhs,rhs]=argn(0);

if rhs==0 
   load(plotlibpath()+'dinosaure.dat')
   h=gcf();
   IMD=h.immediate_drawing;
   h.immediate_drawing='off';
   hdl=trisurf(nodes,x,y,z,rand(z));
   axis equal
   shading interp
   h.immediate_drawing=IMD;
else
   hdl=_mainPlot('trisurf',varargin);
end

endfunction /////
