function out=parseMarker(typeOfPlot,value,pptystring,ppty)

select ppty
case "Marker"
  if type(value)==10
    if length(value)==1
      if ~strindex('.+x*do<>^vs',value)~=[]
        _error(sprintf("%s : %s is not a Marker identifier",typeOfPlot,value))
      end
    elseif and(value~=['none','pentagram'])
        _error(sprintf("%s : %s is not a Marker identifier",typeOfPlot,value))
    end
  else
    _error(sprintf("%s : missing Marker identifier",typeOfPlot))
  end
case "MarkerSize"
  if type(value)==1
    value=abs(value);
  else
    _error(sprintf('%s : missing MarkerSize value',typeOfPlot));  
  end
end

out=list(ppty,value);

endfunction
