function out=_pretty_lims(db,tight)
  _max=db(2);
  _min=db(1);
  if (_max-_min)<%eps
    if abs(_min)<%eps
        _min=-1; _max=1;
    elseif _min<0
        _max=0;
        _min=2*_min;
    else
        _min=0;
        _max=_max*2;
    end
    out=[_min _max];
  elseif ~tight
//    magnitude=10^(int(log10(_max-_min)));
//    out=[floor(_min/magnitude);ceil(_max/magnitude)]*magnitude;
    [_min,_max]=graduate(_min,_max);
    out=[_min,_max];    
  else
    out=db;  
  end
endfunction
