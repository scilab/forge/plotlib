function hdl=_fill3(varargin)

[lhs,rhs]=argn(0);

if rhs==0
  h=gcf();
  IMD=h.immediate_drawing;
  h.immediate_drawing='off';
      x=[0 1 1 0]';y=[0 0 1 1]';c=[1 2 3 4]';
  hdl=_fill3([x 1+x 1+x x],[y y 1+y 1+y],[x y 1-x 1-y],[c 2+c 3+c 4+c],'facecolor','interp');
  _axis equal
  _view(-45,60);  
  h.immediate_drawing=IMD;
else
   hdl=_mainPlot('fill3',varargin);
end

endfunction /////
