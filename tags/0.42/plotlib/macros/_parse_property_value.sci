function [_entity,ax_ppty_value_list,_next]=_parse_property_value(argList,_start,_entity_in)
  
global axesPropertiesNames leafPropertiesNames
    
_next=_start;
_entity=_entity_in;
ax_ppty_value_list=list();

typeOfPlot=_entity.typeOfPlot;

while _next <= length(argList)

  if type(argList(_next))~=10
    break;
  else    
    [_entity,fail]=getColorNumber_new(argList(_next),_entity); 
  end    

  if ~fail
      _next=_next+1;
  elseif _next == length(argList)
      _error(sprintf('%s : unknown property name ""%s"" or missing value',typeOfPlot,argList(_next)));
  else    
      pptystring=convstr(argList(_next));
      value=argList(_next+1);

      if or(pptystring==axesPropertiesNames(1))
      
            parseFunction=axesPropertiesNames(pptystring)(1);
            ppty=axesPropertiesNames(pptystring)(2);              
            cmd=sprintf("%s(typeOfPlot,value,""%s"",""%s"")",parseFunction,pptystring,ppty);
            _ppty_value_list=evstr(cmd);
            ax_ppty_value_list=lstcat(ax_ppty_value_list,_ppty_value_list);
     
      elseif or(pptystring==leafPropertiesNames(1))
      
            parseFunction=leafPropertiesNames(pptystring)(1);
            ppty=leafPropertiesNames(pptystring)(2);              
            cmd=sprintf("%s(typeOfPlot,value,""%s"",""%s"")",parseFunction,pptystring,ppty);
            _ppty_value_list=evstr(cmd);
 
            for i=1:2:length(_ppty_value_list);
               _entity(_ppty_value_list(i))=_ppty_value_list(i+1);
            end

      else
           _error(sprintf('%s : ""%s"" is an unknown property name',typeOfPlot,pptystring));
      end // select convstr(_pair(1))

     _next=_next+2;

  end

end  
  
endfunction
