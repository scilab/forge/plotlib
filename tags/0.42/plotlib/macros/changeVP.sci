function [bnds,cbech]=changeVP(ax,vp)    

margin=ax.parent.user_data.margin;

x=vp(1);y=vp(2);
w=vp(3);h=vp(4);

DIV=30;
dxcb=w/DIV;
dycb=h/DIV;

LX=w-(1+w)*margin;
LY=h-(1+h)*margin;
X=x+(1-x)*margin
Y=y+(1-y)*margin;

cb=ax.user_data.colorbarPosition
cbech=[];

if cb=="left"
  if ax.user_data.YAxisLocation=="left"
    dxcb=dxcb+0.05;
  end
  cbech=[X Y w/DIV LY];	
  X=X+w/DIV+dxcb;
  LX=LX-w/DIV-dxcb;
elseif cb=="right"
  if ax.user_data.YAxisLocation=="right"
    dxcb=dxcb+0.05;
  end
  cbech=[X+LX-w/DIV Y w/DIV LY];	
  LX=LX-w/DIV-dxcb;
elseif cb=="top"
  if ax.user_data.XAxisLocation=="top"
    dycb=dycb+0.01;
  end
  cbech=[X Y LX h/DIV];
  Y=Y+h/DIV+dycb;
  LY=LY-h/DIV-dycb;
elseif cb=="bottom"
  if ax.user_data.XAxisLocation=="bottom"
    dycb=dycb+0.01;
  end
  cbech=[X Y+LY-h/DIV LX h/DIV];
  LY=LY-h/DIV-dycb;
end

bnds=[X Y LX LY];



endfunction
