function %pltlibH_set(h,varargin)
  global leafPropertiesNames axesPropertiesNames figurePropertiesNames
  
  if is_handle_valid(h.handle)
    for i=1:size(h.handle,'*')
      typ(i)=typeof(get(h.handle(i),'user_data'));
    end
    if size(unique(typ),'*')>1
      error('set : handles must be of the same type')  
    end 
  else
    error('set : handle is no more valid');
  end
  
  select typ(1)
  case 'figureData'
    propertiesNames=figurePropertiesNames;
    typ='Figure';
    parse=_parse_figure_ppty_value;
    update=_update_figure;
  case 'axesData'
    propertiesNames=axesPropertiesNames;
    typ='Axes'
    parse=_parse_axes_ppty_value;
    update=_update_axes;
  case 'leafData'
    propertiesNames=leafPropertiesNames;
    typ='Leaf';
    parse=_parse_leaf_ppty_value;
    update=_update_leaf;
  else
    propertiesNames=[];
    typ=h.handle.type;
  end
      
  i=1;
  while i<=length(varargin)
    prop=varargin(i);
    if or(convstr(prop)==propertiesNames(1))
      [_ppty_value_list,i]=parse("set",varargin,i);
      for j=1:size(h.handle,'*')
        update(h(j).handle,_ppty_value_list);
      end
    else
      try
         set(h.handle,prop,varargin(i+1));
      catch
        	_error(sprintf('set : %s is an unknown %s property name',prop,typ))     
      end
      i=i+2;
    end
  end  
  
endfunction
  
