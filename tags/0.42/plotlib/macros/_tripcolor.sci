function hdl=_tripcolor(varargin)

global PLOTLIB

[lhs,rhs]=argn(0);

if rhs==0
   load(PLOTLIB+'tridem.dat')
   h=gcf();
   IMD=h.immediate_drawing;
   hdl=_tripcolor(nodes,xy(1,:),xy(2,:),P(:,1)');
   _shading interp
   _colorbar on
   _axis equal
   h.immediate_drawing=IMD;
else
   hdl=_mainPlot('tripcolor',varargin);
end

endfunction /////
