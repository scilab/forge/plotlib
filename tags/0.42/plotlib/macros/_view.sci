function varargout=_view(varargin) 

[lhs,rhs]=argn(0);

_vect=[];
out=list();

i=1;
ax=get('current_axes');
while i<=length(varargin)
   if typeof(varargin(i))=='pltlibH'
     if varargin(i).handle.type=='Axes'
       ax=varargin(i).handle;
     else
      _error('axis : handle should be an axes handle')
     end
     i=i+1;
 elseif length(varargin)>i
     out=parseView('view',[varargin(i),varargin(i+1)],'view','View')
     i=i+2;
     break;
   else
     out=parseView('view',varargin(i),'view','View')
     i=i+1;
     break;
   end 
end

if i<= length(varargin)
   _error('view : too many input arguments')
end

_update_axes(ax,out);

if lhs==1
  varargout(1)=ax.user_data.View;  
elseif lhs==2
  varargout(1)=ax.user_data.View(1);
  varargout(1)=ax.user_data.View(2);  
end

endfunction

