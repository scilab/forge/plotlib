function hdl=_semilogy(varargin)

[lhs,rhs]=argn(0);

if ~rhs
   w=logspace(-2,2,200);
   s=%i*w;
   g=1../(s.^2+0.01*s+1);
   hdl=_semilogy(w,abs(g));
   return
end


hdl=_mainPlot('semilogy',varargin);

// end of semilogy
endfunction
