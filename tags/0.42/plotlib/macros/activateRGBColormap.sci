function activateRGBColormap(win)
      
  if win.user_data.Colormaptable==[]
    RGBmap=win.user_data.Colormap;
    n1=size(win.user_data.FixedColors,1);
    n2=size(RGBmap,1);
    win.user_data.Colormaptable=n1+1:n1+n2;
    win.color_map=[win.user_data.FixedColors;RGBmap];
  end
  
endfunction

