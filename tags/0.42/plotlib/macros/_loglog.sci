function hdl=_loglog(varargin)

[lhs,rhs]=argn(0);

if ~rhs
   w=logspace(-2,2,512);
   s=%i*w;
   g=[];
   leg=[];
   for alpha=logspace(-2,1,5);
     g=[g;(1)./(s.^2+alpha*s+1)];
     leg=[leg;sprintf('alpha=%4.2f',alpha)];  
   end  
   hdl=_loglog(w,abs(g));
//   legend(leg)
   return
end

hdl=_mainPlot('loglog',varargin);

// end of loglog
endfunction
