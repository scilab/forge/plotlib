<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet 
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    version="1.0" 
    exclude-result-prefixes="exslt str">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
  
  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <sciml>
      <xsl:apply-templates/>
    </sciml>  
  </xsl:template>
  
  <xsl:template match="restrictions"/>

  <xsl:template match="restriction[@refid]" mode="set">
    <xsl:apply-templates select="//restriction[@id=current()/@refid]" mode="set"/>        
  </xsl:template>
  
   <xsl:template match="restriction[@type='string']" mode="set">
   <ifthenelse>
     <condition>
       <equal>
         <op>
           <function-call name="typeof">
             <inputs>
               <parm>value</parm>
             </inputs>
           </function-call>
         </op>
         <op><string>string</string></op>           
       </equal>  
     </condition> 
     <if/>
     <else>
       <macro-call name="error">
         <inputs>
           <parm>
             <function-call name="sprintf">
               <inputs>
                 <parm>
                   <string>Error using set : bad value for %s property '%s' </string>
                 </parm>
                 <parm>class</parm>
                 <parm>ppty</parm>  
               </inputs>
             </function-call>
           </parm>
         </inputs>
       </macro-call>  
     </else>    
   </ifthenelse>    
  </xsl:template>

   
  <xsl:template match="restriction[@type='string' and value]" mode="set">
    <assign>
      <lhs>values</lhs>
      <rhs>
        <list sep=";">
          <xsl:for-each select="value">
            <parm>
              <string>
                <xsl:value-of select="."/>
              </string>
            </parm>
          </xsl:for-each>      
        </list>  
      </rhs>
     </assign>
     <ifthenelse>
       <condition>
             <function-call name="exists">
               <inputs>
                 <parm><string>value</string></parm>
                 <parm><string>local</string></parm>
               </inputs>
             </function-call>  
       </condition> 
       <if>
         <switch expr="value">
           <xsl:for-each select='value'>
             <case value="'{.}'"/>
           </xsl:for-each>
           <else>
             <macro-call name="error">
               <inputs>
                 <parm>
                   <function-call name="sprintf">
                     <inputs>
                       <parm>
                         <string>Error using set : bad value for %s property '%s' </string>
                       </parm>
                       <parm>class</parm>
                       <parm>ppty</parm>  
                     </inputs>
                   </function-call>
                 </parm>
               </inputs>
             </macro-call>  
           </else>    
         </switch>
       </if>
       <else>
         <assign><lhs>out</lhs><rhs>values</rhs></assign>
       </else>
     </ifthenelse>      
  </xsl:template>
  
 
   
  <xsl:template match="restriction[@type='constant']" mode="set"/>
            
  <xsl:template match="model">
  <xsl:param name="class" select="@class"/>
      <xsl:if test="@status!='virtual'">
        <file name="{concat('Classes','/',$class,'/','create',@class,'.sci')}">
          <function-definition name="{concat('create',@class)}"/>
        </file>  
      </xsl:if> 
      <xsl:apply-templates>
        <xsl:with-param name="class" select="$class"/>
       </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template name="setget">
    <xsl:param name="class"/>
      <file name="{concat('Classes','/',$class,'/','set',$class,@name,'.sci')}">
          <function-definition name="{concat('get',$class,@name,'Property')}">
            <inputs>
              <parm>class</parm>
              <parm>h</parm>
              <parm>ppty</parm>
            </inputs>
            <outputs>
              <parm>out</parm>
            </outputs>
            <body>
              <xsl:if test="not(@storage) or @storage='yes'">
                <assign>
                <lhs>user_data</lhs>
                <rhs>
                  <function-call name="get">
                    <inputs>
                      <parm>h.handle</parm>
                      <parm><string>user_data</string></parm>
                    </inputs>
                  </function-call>
                </rhs>
                </assign>
                <assign>
                  <lhs>out</lhs>
                  <rhs>
                    <function-call name="user_data">
                      <inputs>
                        <parm>ppty</parm>
                      </inputs>
                    </function-call>     
                  </rhs>
                </assign>
              </xsl:if>
            </body>
           </function-definition>   
           <function-definition name="{concat('set',$class,@name,'Property')}">
           <inputs>
              <parm>class</parm>
              <parm>h</parm>
              <parm>ppty</parm>
              <parm>value</parm>
            </inputs>
            <outputs>
              <parm>out</parm>
            </outputs>
            <body>
              <xsl:apply-templates mode="set"/>
            </body>  
           </function-definition>       
         </file>
  </xsl:template>
  
  <xsl:template name="equivalence">
    <xsl:param name="class"/>
    <xsl:param name="class2" select="../@class"/>
    <xsl:param name="name2" select="@name"/>
     <assign>
      <lhs>
        <xsl:value-of select="concat('get',$class,@name,'Property')"/>
      </lhs>
      <rhs>
        <xsl:value-of select="concat('get',$class2,$name2,'Property')"/>   
      </rhs>  
    </assign>    
    <assign>
      <lhs>
        <xsl:value-of select="concat('set',$class,@name,'Property')"/>
      </lhs>
      <rhs>
        <xsl:value-of select="concat('set',$class2,$name2,'Property')"/>   
      </rhs>  
    </assign> 
  </xsl:template>
  
  <xsl:template match="group">
    <xsl:param name="class"/>
    <xsl:if test="$class=../@class">
      <xsl:call-template name="setget">
        <xsl:with-param name="class" select="$class"/>
      </xsl:call-template>      
    </xsl:if>
    <xsl:apply-templates select="property">
      <xsl:with-param name="class" select="$class"/>
    </xsl:apply-templates>
  </xsl:template>
    
    
  <xsl:template match="property">
    <xsl:param name="class"/>
    <xsl:choose>
    <xsl:when test="$class=../@class">
      <xsl:call-template name="setget">
        <xsl:with-param name="class" select="$class"/>
      </xsl:call-template>      
    </xsl:when>
    <xsl:when test="//model[@class=$class]/@status!='virtual'">
      <xsl:call-template name="equivalence">
        <xsl:with-param name="class" select="$class"/>
      </xsl:call-template>
    </xsl:when>
    </xsl:choose>
  </xsl:template>
  
    <xsl:template match="group/property">
    <xsl:param name="class"/>
      <xsl:call-template name="equivalence">
      <xsl:with-param name="class" select="$class"/>
      <xsl:with-param name="class2" select="../../@class"/>
      <xsl:with-param name="name2" select="../@name"/>
    </xsl:call-template>
  </xsl:template>

      
  <xsl:template match="inherits">
      <xsl:param name="class" select="../@class"/>
      <xsl:apply-templates select="//model[@class=current()/@class]">
        <xsl:with-param name="class" select="$class"/>
      </xsl:apply-templates>
 </xsl:template>
      
</xsl:stylesheet>
 