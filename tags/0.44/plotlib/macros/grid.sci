function grid(varargin) 

[lhs,rhs]=argn(0);

[lhs,rhs]=argn(0);
vect=[];

i=1;
ax=get('current_axes');
while i<=length(varargin)
   if typeof(varargin(i))=='pltlibH'
     if varargin(i).handle.type=='Axes'
       ax=varargin(i).handle;
     else
      _error("axis : handle should be an axes handle")
     end
     i=i+1;
   elseif type(varargin(i))==10
      out=parseOnOff("grid",varargin(i),"grid","");
      break;
   end 
end

if ~exists("out","local")
  cmd="toggle";
else
  cmd=out(2);
end

if rhs==1  
  if varargin(1)=='on'
    cmd="on";
  elseif varargin(1)=='off'
    cmd="off";
  else
    _error('hold : unknown hidden state (must be ''on'' or ''off'')')
  end
elseif rhs==0
  cmd="toggle";
end

out=list();

for ppty=["XGrid" "YGrid" "ZGrid"]
    _done=%f;
    if cmd=="on" | cmd=="toggle"
        if ax.user_data(ppty)=="off"
           out=lstcat(out,ppty,"on");
           _done=%t;
        end
    end  
    if cmd=="off" | (cmd=="toggle" & ~_done)
        if ax.user_data(ppty)=="on"
           out=lstcat(out,ppty,"off");
         end
    end
end

_update_axes(ax,out);

endfunction
