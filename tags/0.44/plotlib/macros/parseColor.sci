function out = parseColor(typeOfPlot,value, pptystring, ppty)

//
// Parsing function for the 'background' or 'foreground' property 
//

rgb=[];
select type(value)
case 1 // a matrix (must be a 3 element vector)
   if length(value)~=3
      _error(sprintf('%s : %s color spec must be a 3 element vector',typeOfPlot,pptystring));
   end
case 10 // a string
  if or(pptystring==['color';'backgroundcolor']) & value~='none'
    rgb=name2rgb(value)/255;  
  elseif pptystring=='facecolor' & and(value~=['none';'default';'flat';'interp';'mean'])
    rgb=name2rgb(value)/255;
  elseif pptystring=='edgecolor' & and(value~=['none';'default'])
    rgb=name2rgb(value)/255;      
  elseif or(pptystring==['markerfacecolor';'markeredgecolor']) & and(value~=['none';'auto'])
    rgb=name2rgb(value)/255;      
  end
else
    _error(sprintf('%s : missing %s color spec',typeOfPlot,pptystring));
end

if type(value)==10 & and(value~=['none';'default';'flat';'interp';'mean'])
  if rgb==[]  
    printf('\n%s : ''%s'' is an unknown %s specification, using ''default'' instead.\n',typeOfPlot,value,ppty);
    value='default'     
  else
    value=rgb;    
  end
end

out=list(ppty,value);

endfunction
