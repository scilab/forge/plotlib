function varargout=axes(varargin)

global defaultFigureUserData defaultAxesUserData

[lhs,rhs]=argn();

_start=1;
if length(varargin)
   if typeof(varargin(_start))=="pltlibH"
     if varargin(_start).handle.type=="Axes"
       h=varargin(_start).handle
     else
      _error("axes : handle should be an axes handle")
     end
     sca(h);
     varargout(1)=[];
     return;
   end     
end

win=get('current_figure');

IMD=win.immediate_drawing;
win.immediate_drawing="off";

ax=newaxes();

[_axes_ppty_value_list,_next]=_parse_axes_ppty_value("axes",varargin,_start);
_update_axes(ax,_axes_ppty_value_list,%t);

varargout(1)=mlist(['pltlibH','handle'],ax);

win.immediate_drawing=IMD;

endfunction
