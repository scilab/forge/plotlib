function _add_event_handler(win,_function_name)

global eventHandlers

if type(win)==1
  h=scf(win);
else
  h=win;
  win=h.figure_id;
end

h.user_data.eventHandlers($+1)=_function_name;
eventHandlers(win+1)=h.user_data.eventHandlers;

endfunction
