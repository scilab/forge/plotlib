function setColors(_entity,h)

global _SHADED_ENTITIES
  
_type=h.typeOfPlot;
ax=h.parent;
_win=ax.parent;

if or(_type==['mesh';'trimesh'])
  _entity.surface_mode='on';
  if h.FaceColor=='default'
   _entity.color_mode=findColorIndex(_win,ax.user_data.Color);
  elseif h.FaceColor~='none'
    _entity.color_mode=findColorIndex(_win,h.FaceColor);
  else
     _entity.color_mode=0;
  end  
  c=h.EdgeColor;
  if c=='default'
    c=1-ax.user_data.Color;     
    _entity.foreground=findColorIndex(_win,c);
  elseif c=='none'
    if h.FaceColor=='none'
      _entity.surface_mode='off';
    else
      _entity.color_mode=-_entity.color_mode;
    end
  elseif type(c)==1
     _entity.foreground=findColorIndex(_win,c);
  end
  _entity.hiddencolor=-1;
  _entity.color_flag=0;

elseif or(_type==_SHADED_ENTITIES)

  if h.EdgeColor=='none'
     _entity.color_mode=-1;
  else
    c=h.EdgeColor;
    if c=='default'
      if ax.user_data.Color=='none'
        c=[0 0 0];
      else
        c=1-ax.user_data.Color; 
      end
    end
    _entity.color_mode=1;       
    _entity.foreground=findColorIndex(_win,c);
  end
  _entity.hiddencolor=-1;
  if or(h.FaceColor==['default';'flat']) 
    _entity.color_flag=4;  // Matlab flat shading
  elseif h.FaceColor=='mean' 
    _entity.color_flag=2;  // Scilab flat shading
  elseif h.FaceColor=='interp'   
    _entity.color_flag=3;          
  elseif type(h.FaceColor)==1
    _entity.color_flag=0;            
    _entity.color_mode=findColorIndex(_win,h.FaceColor);
  end

end

endfunction

