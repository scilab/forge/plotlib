h=figure(0);
drawlater;
clf;
global PLOTLIB
load(PLOTLIB+'mandrill.dat');
n=size(map,1);
colormap([map
map*diag([1 0 0]);
map*diag([0 1 0]);
map*diag([0 0 1])]);
for i=1:4
  subplot(2,2,i);
  drawlater
  image(X+(i-1)*n);
  axis equal
  axis tight
  drawnow
end
  
