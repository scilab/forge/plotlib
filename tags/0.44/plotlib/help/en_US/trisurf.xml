<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Ajouter ici d'éventuels commentaires sur le fichier XML
-->
<refentry version="5.0-subset Scilab" xml:id="trisurf" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 26-05-2009 $</pubdate>
  </info>

  <refnamediv>
    <refname>trisurf</refname>

    <refpurpose>Display surface defined by a triangulation</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>hdl = trisurf(TRI,X,Y,Z,C)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameter</title>

    <variablelist>
      <varlistentry>
        <term>TRI</term>

        <listitem>
          <para>3 by n matrix. A row of TRI contains indexes into the X,Y, and
          Z vertex vectors to define a single triangular face</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>X</term>

        <listitem>
          <para>1 by n matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Y</term>

        <listitem>
          <para>1 by n matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Z</term>

        <listitem>
          <para>1 by n matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>C</term>

        <listitem>
          <para>1 by n matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>hdl</term>

        <listitem>
          <para>Handle of the created Fac3d entity</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>trisurf(TRI,X,Y,Z,C) displays the triangles defined in the 3-by-n
    face matrix TRI as a surface. A row of TRI contains indexes into the X,Y,
    and Z vertex vectors to define a single triangular face. The color is
    defined by the vector C.</para>

    <para>trisurf(TRI,X,Y,Z) uses C = Z, so color is proportional to surface
    height.</para>

    <para>trisurf(...,'param','value','param','value'...) allows additional
    param/value pairs to be used. See mesh, surf, surfl for the possible
    param/value pairs.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">global PLOTLIB
load(PLOTLIB+'tridem.dat');
trisurf(nodes,xy(1,:),xy(2,:),P(:,3)','facecolor','interp');
view(45,45);
colorbar bot;</programlisting>
  </refsection>

  <refsection>
    <title>Screenshot</title>

    <screenshot>
      <mediaobject>
        <imageobject>
          <imagedata fileref="trisurf.png" />
        </imageobject>
      </mediaobject>
    </screenshot>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member>
        <link linkend="mesh">mesh</link>
      </member>

      <member>
        <link linkend="surf">surf</link>
      </member>

      <member>
        <link linkend="surfl">surfl</link>
      </member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Stéphane Mottelet</member>
    </simplelist>
  </refsection>
</refentry>
