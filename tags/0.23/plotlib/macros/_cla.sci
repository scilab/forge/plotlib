function _cla(varargin)

global defaultAxisUserData;

[lhs,rhs]=argn();

cmd=[];
if length(varargin)<1
  ax=gca();
elseif length(varargin)<2
  if type(varargin(1))==10
    ax=gca();
    cmd=varargin(1);
  else
    ax=varargin(1);
  end
elseif length(varargin)<3
  ax=varargin(1);
  cmd=varargin(2);
end

if size(ax.children,1) 
  delete(ax.children);
end
ax.user_data.currentColor=1;

if type(ax.user_data.legendHandle)~=1
    ierr=execstr('delete(ax.user_data.legendHandle)','errcatch');
    ax.user_data.legendType=0;
end

if type(ax.user_data.colorbarHandle)~=1
    ierr=execstr('delete(ax.user_data.colorbarHandle)','errcatch');
    changeVP(gcf(),ax,'off');
end

if cmd=="reset"
    ax=gda();
    ax.user_data=defaultAxisUserData;
end

endfunction
