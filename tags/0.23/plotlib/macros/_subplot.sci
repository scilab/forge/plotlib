function ax=_subplot(n,m,p)

global defaultFigureUserData defaultAxisUserData DONE IMD

[lhs,rhs]=argn();

if rhs==3
    subplotString=string(n)+string(m)+string(p);
elseif rhs==1
    subplotString=string(n);
    if length(subplotString)~=3
        str=sprintf('subplot : arguments must be a number of the type nmp or n,m,p');
        error(str);
    end
    n=evstr(part(subplotString,1));
    m=evstr(part(subplotString,2));
    p=evstr(part(subplotString,3));    
end

if (p<1) | (p>n*m)
  str=sprintf('subplot : number of subplot must be between 1 and %d',n*m);
  error(str);
end

win=safeInit();

matchingAxes=[];
for i=1:size(win.children,1)
    axesHandle=win.children(i);
    if axesHandle.user_data.subplotString==subplotString
        matchingAxes=axesHandle;
    end
end

if type(matchingAxes)==1
    lx=1/m;
    ly=1/n;
    x=int((p-1)/n);
    y=(p-1)-x*n;
    x=x*lx;
    y=y*ly;
    
    ax=gca();
    if size(ax.children,1)~=0
        ax=newaxes();
        ax.visible='off';
    end
    
    ax.user_data=defaultAxisUserData;
    ax.user_data.viewport=[x y lx ly];
    ax.user_data.subplotString=subplotString;
    ax.user_data.currentColor=1;
    ax.foreground=findColorIndex(ax.parent.user_data.foreground);
    ax.background=findColorIndex(ax.parent.user_data.background);
    sca(ax);
else
    sca(matchingAxes);
    ax=matchingAxes;
end

_ind=find(win.user_data.eventHandlers=="plotlib_subplot_handler");
if _ind==[]
	_add_event_handler(win,"plotlib_subplot_handler");
end

endfunction
