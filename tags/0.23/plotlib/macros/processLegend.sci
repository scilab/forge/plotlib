function processLegend(ax,mat,typ)

if size(mat,1)>size(ax.children,1)
    mat=mat(1:size(ax.children,1),:);
    warning("legend : some superfluous labels have been ignored");
end

numberOfLegends=size(mat,1);

if numberOfLegends==0
	return
end

blanc=' ';
mat=blanc(ones(numberOfLegends,1))+mat;

ind=find(mat==' ');
indcol=1:numberOfLegends;
indcol(ind)=[];
mat(ind)=[];

nl=length(indcol);

if nl==0
	return
end

ierr=execstr('delete(ax.user_data.legendHandle)','errcatch');

if typ==-1
else
    hl=scilab_legend(ax,mat,typ);
    hl.font_color=ax.foreground;
    hl.fill_mode='off';
    ax.user_data.legendHandle=hl;
    ax.user_data.legendType=typ;
end
sca(ax);

endfunction
