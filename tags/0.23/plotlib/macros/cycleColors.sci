function  [lineColorIndices,markerForegroundIndices,markerBackgroundIndices]=cycleColors(ax,c,n)

	global MAXCOL

	currCol=ax.user_data.currentColor;

	colorIndices=modulo(currCol+(1:n+1)-2,MAXCOL)+1;
	lineColorIndices=colorIndices(1:$-1);
	markerForegroundIndices=colorIndices(1:$-1);    
	markerBackgroundIndices=zeros(1,n)+ax.background;

	if and(c==[0 0])
    	ax.user_data.currentColor=colorIndices($);
	elseif c(2)==0
    	lineColorIndices=c(ones(1,n),1);
    	ax.user_data.currentColor==colorIndices($);
	elseif c(1)==0
    	markerForegroundIndices=c(ones(1,n),2);    
    	ax.user_data.currentColor==colorIndices($);
    	lineColorIndices=zeros(1,n)+ax.background;
	else
    	lineColorIndices=c(ones(1,n),1);
    	markerForegroundIndices=c(ones(1,n),2);    
	end
	
endfunction
