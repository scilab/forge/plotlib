function hdl=_fill(varargin)

[lhs,rhs]=argn(0);

if rhs==0
	x=[0 1 1 0]';y=[0 0 1 1]';c=[1 2 3 4]';
	hdl=_fill([x 1+x],[y 1+y],[c 2+c],'facecolor','interp','axis','equal')
else
   hdl=mainPlot3d('fill',varargin);
end

endfunction /////
