function [c,m,sizes,lineStyle,fail]=getColorNumber(win,ch) 

c=[0 0];
m=[];
lineStyle=0;
fail=0;
sizes=[1 1];

s=win.user_data.simpleColorTable;

// Line type modifiers

if strindex(ch,'--')
   ch=strsubst(ch,'--','');
   lineStyle=2;
elseif strindex(ch,'-.')
   ch=strsubst(ch,'-.','');
   lineStyle=2;
elseif strindex(ch,'-')
   ch=strsubst(ch,'-','');
   lineStyle=1;
end   

//

k=1;

for i=1:length(ch)
if m~=[]
    k=2;
end 
select part(ch,i)
  case 'y'
     c(k)=[s.y];
  case 'm'
     c(k)=[s.m];
  case 'c'
     c(k)=[s.c];
  case 'b'
     c(k)=[s.b];
  case 'g'
     c(k)=[s.g]
  case 'r'
     c(k)=[s.r];
  case 'w'
     c(k)=[s.w];
  case 'k'
     c(k)=[s.k];
  case '.'
     m=0;
  case '+'
     m=1;
  case 'x'
     m=2;
  case '*'
     m=3;
  case '^'
     m=6;
  case 'v'
     m=7;
  case 't'
     m=8;
  case 'd'
    m=5;
  case 'f'
    m=4;
  case 'o'
     m=9;
  case '1'
     sizes(k)=1;
  case '2'
     sizes(k)=2;
  case '3'
     sizes(k)=3;
  case '4'
     sizes(k)=4;
  case '5'
     sizes(k)=5;
  case '6'
     sizes(k)=6;
  case '7'
     sizes(k)=7;
  case '8'
     sizes(k)=8;
  case '9'
     sizes(k)=9;
  else
     fail=1;
end // select
end // for

if lineStyle == 0 & m==[]
   lineStyle=1;   
end
if lineStyle == 1 & c(1)==0 & c(2) <> 0
  c(1)=c(2);
end

// end of getColorNumber
endfunction
