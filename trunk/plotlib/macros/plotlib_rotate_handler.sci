function out=plotlib_rotate_handler(win,x,y,ibut,in)

  global _catched _ref_angles _ref_position _ref_info 
  
  h=scf(win);
  out=in;
  if or(ibut==[3 0]) // left button pressed or clicked
    
    [res,err]=evstr('is_handle_valid(_catched)');
    if ~res | res==[]
      ax=h.children;
      h_matching=[];
      numberOfAxes=size(ax,1);
      for i=1:numberOfAxes;
        xl=ax(i).axes_bounds(1)*h.axes_size(1);
        w=ax(i).axes_bounds(3)*h.axes_size(1);
        yu=ax(i).axes_bounds(2)*h.axes_size(2);
        _h=ax(i).axes_bounds(4)*h.axes_size(2);
        if (x-xl >= 0) & (x-xl <=w) & (y-yu >=0) & (y-yu<=_h) & ax(i).visible=="on" & type(ax(i).user_data)~=1
          h_matching=ax(i);
          break;
        end
      end
      if length(h_matching)
            _catched=h_matching;
            _ref_angles=h_matching.rotation_angles;
            _ref_position=[x,y,w,_h];
            h.info_message="Plotlib message: left-click to stop rotation.";
      end
    else
      az=modulo(modulo(_catched(1).rotation_angles(2)-270,360)+360,360);
      el=90-_catched(1).rotation_angles(1);
      for i=1:size(_catched,'*')
        _catched(i).user_data.View=[az,el];
      end
      h.info_message=_ref_info;
      _del_event_handler(win,'plotlib_rotate_handler');
      _catched=[];
     end               
     
  elseif ibut==-1 & _catched~=[]// pointer moved  
    
    dx=(x-_ref_position(1))/_ref_position(3);
    dy=(y-_ref_position(2))/_ref_position(4);
    alpha=_ref_angles(1)-dy*270;
    alpha=max(0,min(180,alpha));
    theta=_ref_angles(2)-dx*360;
    az=modulo(modulo(theta-270,360)+360,360);
    el=90-alpha;
    drawlater;
    _catched.rotation_angles=[alpha,theta];    
    drawnow;
    h.info_message=sprintf('Az: %+4.1d El: %+3.1d',az,el);
  elseif ibut~=-1 
    disp(ibut)  
  end
  out=%t;
endfunction
