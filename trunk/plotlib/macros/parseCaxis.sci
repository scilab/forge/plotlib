function out = parseCaxis(typeOfPlot,value,pptystring,ppty)

select type(value)
case 10
      select value
      case "auto"
         out=list(ppty,"auto");
      case "manual"
         out=list(ppty,"manual");
      else
         _error(sprintf("%s : unknown %s mode",typeOfPlot,ppty));
      end
case 1
      if length(value)==2
         out=list(ppty,value(:)');
      else
         _error(sprintf("%s : %s vector must have the form [min max]",typeOfPlot,ppty));
      end
end

endfunction
