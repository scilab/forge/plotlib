function hdl=addPlot(_type,X,Y,Z,c,markerId,sizes,lineStyle,ax,_factor)


// Adds a quadruple to the list of elementary plots to do

[lineColorIndices,markerForegroundIndices,markerBackgroundIndices]=cycleColors(ax,c,size(X,2));

if _type=="plot3"

    if ax.children==[]
      ax.data_bounds=[min(X) min(Y) min(Z);max(X) max(Y) max(Z)];
    end
    param3d1(X,Y,list(Z,lineColorIndices));
    h=gce();

else

    if ax.children==[]
      ax.data_bounds=[min(X) min(Y);max(X) max(Y)];
    end
    plot2d(X,Y,style=lineColorIndices);
    h=get(gce(),'children');

end

if lineStyle==0
    set(h,'line_mode','off');
else
    set(h,'line_style',lineStyle);
end

set(h,'thickness',sizes(1));

if markerId ~=[]
    set(h,'mark_mode','on');
    set(h,'mark_style',markerId);    
    set(h,'mark_size_unit','point');    
    set(h,'mark_size',6+3*(sizes($)-1));  
    for i=1:size(h,1)
        set(h(size(h,1)-i+1),'mark_foreground',markerForegroundIndices(i));
    end
    if lineStyle==0
        for i=1:size(h,1)
            set(h(size(h,1)-i+1),'mark_background',lineColorIndices(i));
        end
	else
		for i=1:size(h,1)
            set(h(size(h,1)-i+1),'mark_background',markerBackgroundIndices(i));
        end
    end
end

hdl=h;

endfunction
