function out=%pltlibH_e(i,h)
  if typeof(i)=='constant'
    try
      out=mlist(['pltlibH','handle'],h.handle(i))
    catch
      error(21);    
    end
  elseif typeof(i)=='string'
    out=get(h,i);
  end
endfunction
