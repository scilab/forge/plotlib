function hdl=pcolor(varargin)

[lhs,rhs]=argn(0);

if rhs==0
   colormap(jetcolormap(128));
   r=linspace(0,1,25);
   theta=%pi/4+linspace(0,3*%pi/2,25);
   [R,T]=meshgrid(r,theta);
   h=gcf();
   IMD=h.immediate_drawing;
   h.immediate_drawing='off'; 
   hdl=pcolor(R.*cos(T),R.*sin(T),rand(T,'normal'),'facecolor','interp');
   colorbar right;
   axis equal;
   h.immediate_drawing=IMD; 
else
   hdl=_mainPlot('pcolor',varargin);
end

endfunction /////
