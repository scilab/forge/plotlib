function hdl=tripcolor(varargin)

[lhs,rhs]=argn(0);

if rhs==0
   load(plotlibpath()+'/tridem.sod')
   h=gcf();
   IMD=h.immediate_drawing;
   hdl=tripcolor(nodes,xy(1,:),xy(2,:),P(:,1)');
   shading interp
   colorbar on
   axis equal
   h.immediate_drawing=IMD;
else
   hdl=_mainPlot('tripcolor',varargin);
end

endfunction /////
