function h=legend(varargin)

win=get('current_figure');
ax=get('current_axes');
h=[];

IMD=win.immediate_drawing;
win.immediate_drawing='off';
i=1;
while i<=length(varargin)
   if typeof(varargin(i))=='pltlibH'
     if varargin(i).handle.type=='Axes'
       ax=varargin(i).handle;
     else
      _error('legend : handle should be an axes handle')
     end
     i=i+1;
   elseif typeof(varargin(i))=='string'
    select varargin(i)
    case 'show'
      h=processLegend(ax);
    case 'hide'      
      if type(ax.user_data.legendHandle)~=1
          ax.user_data.legendHandle.visible='off';
      end  
    else
      [mat,nbproc,typ]=parseLegend('legend',varargin,i);
      h=processLegend(ax,mat,typ);
      break;
    end
    i=i+1;
  end 
end
 
win.immediate_drawing=IMD;

endfunction
