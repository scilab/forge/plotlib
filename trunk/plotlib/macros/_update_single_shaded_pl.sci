function _update_single_shaded_pl(h)  
  global %plotlib_global

  C=computeColor(h.user_data);
  if or(h.user_data.typeOfPlot==%plotlib_global._SHADED_ENTITIES)
    h.data.color=C(h.user_data.vertex_indices);
  else
    h.data=C($:-1:1,:);
  end
endfunction
