function hdl=trisurfl(varargin)

[lhs,rhs]=argn(0);

if rhs==0
   colormap(greencolormap(128));
   load(plotlibpath()+'/dinosaure.sod')
   h=gcf();
   IMD=h.immediate_drawing;
   h.immediate_drawing='off'
   hdl=trisurfl(nodes,x,y,z)
   axis equal
   shading interp
   colormap green
   h.immediate_drawing=IMD;
else
   hdl=_mainPlot('trisurfl',varargin);
end

endfunction /////
