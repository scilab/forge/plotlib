mode(-1)
//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//

demopath = get_absolute_file_path("plotlib.dem.gateway.sce");

subdemolist=['pcolor'
'tripcolor'
'surf'
'surfl'
'trisurf'
'trisurfl'
'mesh'
'trimesh'
'triplot'
'quiver'
'quiver3'
'plot3'
'fill'
'fill3'
'loglog'
'plotyy'];

subdemolist(:,2)=demopath+subdemolist(:,1)+'.sce';
subdemolist=[subdemolist;
'subplot',demopath+filesep()+'subplot_demo.sce'
'image',demopath+filesep()+'image_demo.sce'];

